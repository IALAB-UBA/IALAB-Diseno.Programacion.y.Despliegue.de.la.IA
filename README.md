# **Curso IALAB - Diseño Programacion y Despliegue de la IA**

Curso de Inteligencia Artificial - Diseño Programacion y Despliegue de la IA.

## **Unidad 1: Conceptos generales y Entorno de Desarrollo Integrado (IDE) - Primeros pasos en la IDE**

- Contenido de la Unidad 1

1. ¿Qué es la estadística?
2. ¿Qué es machine learning? Diferencias entre ML y estadística
3. Hacia una convergencia natural
4. ¿Qué es un ETL y cómo se usa?
5. ¿Qué es una base de datos? Tipos de bases de datos
6. ¿Qué es un Data Warehouse?
7. ¿Qué es un Data Lake?
8. Diferencias entre un DW y DL
9. Consumos de información
10. ¿Qué es la visualización?
11. Cuadros de repaso de algoritmos en Machine Learning
12. IA fiable: Revisión
13. Topic Modeling: Revisión
14. Support Vector Machines: Revisión
15. Sistemas de recomendación: Revisión
16. Integración y Revisión de Conceptos
17. Los sistemas conexionistas – Representaciones subsimbólicas
18. Dando luz a discusiones éticas
19. Los sistemas conexionistas: Tipos de aprendizaje automático
20. El error: Modelos rígidos y flexibles
21. Objetivo: Mayor poder predictivo
22. Regularización en el descenso por el gradiente
23. Lasso en el descenso por el gradiente
24. No supervisados: Clustering con K-Means
25. No supervisados (Redes neuronales)
26. Modelos de mapas autoorganizados (SOM) Kohonen – Clustering
27. Aprende a responder asertivamente basado en el recuerdo
28. Restricted Boltzmann Machines
29. Cierre del repaso

- Test de lectura 1

## **Unidad 2: Nociones de CRISP-DM - Visualización, plataformas, tecnologías, métricas**

- Contenido de la Unidad 2

1. Descubrimiento de Conocimiento basado en la Exportación de la Información
2. Diferencias entre Big Data, Business Intelligence y Business Analytics
3. Definiciones "no hegemónicas"
4. El Proceso de la Minería de Datos
5. Minería de datos frente a Big Data
6. Knowledge Discovery in Database (KDD)
7. OLAP (Proceso Analítico Online)
8. Fases de la Ingeniería de Explotación de la Información (parte 1)
9. CRISP-DM
10. Fases de la Ingeniería de Explotación de la Información (parte 2)
11. Data Science y Machine Learning
12. Algoritmos inteligentes
13. Diseño de procesos inteligentes: Descubrimiento de reglas de comportamiento
14. Descubrimiento de grupos
15. Descubrimiento de atributos significativos o interdependencia
16. Descubrimiento de reglas de pertenencia a los grupos
17. Proceso de ponderación de las características de las reglas de comportamiento o de los atributos de los agrupamientos
18. Ejemplos de lo visto
19. Overview of clustering

- Test de lectura 2

## **Unidad 3: Aprendizaje Supervisado - Tecnologías Asociadas y Arquitecturas avanzadas de IA **

- Contenido de la Unidad 3

1. Procesos de descubrimientos de reglas de comportamiento
2. Ingeniería de explotación de la información
3. Lo que ya conocemos de árboles de decisión
4. Construcción de árboles de decisión
5. El poder de los árboles
6. Entropía y Ganancia de información
7. Concepto de Complejidad
8. Modelos DT: Aprendizaje Supervisado
9. Modelos DT: Nodos
10. Modelos DT: Interpretaciones
11. Aplicaciones de modelos de TDIDT
12. Trabajando con Sklearn en árboles de decisiones para clasificaciones
13. Ejemplo con Dataset Iris de Sklearn
14. Otro tipo de árbol de inducción: Regresión
15. Los modelos que implementa Sklearn

- Test de lectura 3

## **Unidad 4: Random Forest - CART - C4.5 - SVM - KNN - Perceptron MLP - Deep Learning - RNN-CNN, YOLO, entre otros**

- Contenido de la Unidad 4

1. Completando la fase de los procesos de descubrimiento en la ingeniería de explotación de la información
2. Ciclo de vida de la Ciencia de Datos
3. Ingeniería de explotación de la información
4. Herramientas
5. Modelos Random Forest
6. Descubrimiento de grupos (parte 1)
7. Descubrimiento de grupos (parte 2)
8. Ejemplo de uso de un Mapa Autoorganizado (SOM) de Kohonen en R
9. Scatter Plot de parejas de vectores de datos
10. Descubrimiento de atributos significativos o interdependencia (parte 1)
11. Descubrimiento de atributos significativos o interdependencia (parte 2)
12. Algunas herramientas más usadas: Tanagra
13. Algunas herramientas más usadas: Knime
14. Algunas herramientas más usadas: Apache Spark
15. Algunas herramientas más usadas: Rapidminer
16. Algunas herramientas más usadas: Power BI de Microsoft

- Test de lectura 4

## **Unidad 5: Tecnologías Asociadas y Arquitecturas avanzadas de IA**

- Contenido de la Unidad 5

1. Límites de análisis estadísticos simples
2. Predicciones
3. Correlaciones
4. Correlaciones: Peligros
5. Más peligros: La Paradoja de Simpson
6. Razonamiento bajo incertidumbre: Conocimiento incierto vs. incompleto
7. ¡Ojo con los cálculos probabilísticos!
8. Factores causales: Decisiones bajo incertidumbre
9. Factores causales
10. Causalidad. Ejemplo
11. Análisis causal del riesgo
12. Razonamiento bayesiano
13. Razonamiento bayesiano sin fórmulas
14. Redes bayesianas
15. Construcción y uso de Redes Bayesianas: Resumen

- Test de lectura 5

## **Unidad 6: Clustering - Aprendizaje no supervisado - Tecnologías Asociadas y Arquitecturas avanzadas de IA**

- Contenido de la Unidad 6

1. Deep Learning
2. Convolutional Neuronal Networks
3. CNN Filtros y Max Pooling
4. CNN
5. TensorFlow
6. Recurrent Neural Network
7. Ejemplo de cierre: Caso PretorIA (parte 1)
8. Ejemplo de cierre: Caso PretorIA (parte 2)
9. Recommendations worth a million: an introduction to clustering

- Test de lectura 6
